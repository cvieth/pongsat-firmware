/*
	Pongsat FW Rev. 1.0
	fuer Atmega 168 / 328 (V)
	Tacktung: 8 Mhz
	Versorgung aus einer LiPo-Zelle, 4,1 V - 2,8 V, 300mAh
	
	am I2C Bus sind folgende Sensoren:
	* ITG-3200 Gyro		                (2,10 V - 3,60 V, absolut Maximum 6,00 V)
	* BMA180 Beschleunigungssensor		(1,62 V - 3,60 V, absolut Maximum 4,25 V)
	* BMP085 Luftdrucksensor		(1,80 V - 3,60 V, absolut Maximum 4,25 V)
	* HMC5883L Magnetfeldsensor		(2,16 V - 3,60 V, absolut Maximum 4,80 V)
	
	am 1-Wire Bus sind folgende Sensoren:
	* Maxim DS18B20 an Außenhaut            (3,00 V - 5,50 V)
	* Maxim DS18B20 am Lipo                 (3,00 V - 5,50 V)
	
	HW Setup:
	Arduin-Pin	Atmega-Pin	Funktion
	0		PD0		RX
	1		PD1		TX
        2               PD2             1-Wire
        3               PD3             Jumper1
        4               PD4             Jumper2
	10		PB2		CS-Flash
	11		PB3		MOSI
	12		PB4		MISO
	13		PB5		SCK
	18/ADC4		PC4		SDA
	19/ADC5		PC5		SCL
	
	ADCs:
	* keine
	
	Funktionen:
	* Alle 10 sec Messwerte einlesen und in ext. Flash ablegen
	
	Todo:
	* Lesen / Schreiben ins Flash testen
	* Flashwrite auf Lineend als Ende umbauen
*/
#include <dataflash.h> // http://arduino.cc/playground/Code/Dataflash
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include <avr/eeprom.h> 

// Pins Jumper
#define jpin1 3		
#define jpin2 4			
unsigned int mode = 0;

struct config_t
{
    long adress;
} flashadress;

// Voltage Variable
long voltage = 0;

// Dataflash defines
int j = 0;
int i = 0;
char messageline[200];
char int_string[10];
//PROGMEM int lastpage=0; 			// last page written to (progmem, ram sparen)
PROGMEM long pages=4096; 			//total pages that will be used (progmem, ram sparen)
Dataflash dflash; 

// One wire defines
#define ONE_WIRE_BUS 3
#define TEMPERATURE_PRECISION 12
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress insideThermometer, outsideThermometer;

// Sensor Variablen / Defines
#define ROLL       0
#define PITCH      1
#define YAW        2

static uint32_t currentTime = 0;
static int16_t  i2c_errors_count = 0;
static uint16_t calibratingA = 0;  // the calibration is done is the main loop. Calibrating decreases at each cycle down to 0, then we enter in a normal mode.
static uint8_t  calibratingM = 0;
static uint16_t calibratingG;
static uint16_t acc_1G;             // this is the 1G measured acceleration
static int16_t  acc_25deg;
static uint8_t  nunchuk = 0;
static int16_t  accTrim[2] = {0, 0};
static int16_t  gyroADC[3],accADC[3],magADC[3];
static int16_t  gyroData[3] = {0,0,0};
static int16_t  gyroZero[3] = {0,0,0};
static int16_t  accZero[3]  = {0,0,0};
static int16_t  magZero[3]  = {0,0,0};
static int32_t  pressure;
static int32_t  BaroAlt;
static int16_t  angle[2]    = {0,0};  // absolute angle inclination in multiple of 0.1 degree    180 deg = 1800
static int8_t   smallAngle25 = 1;

//VCC gegen interne 1,1 V Bandgapreferenz messen
long readVcc() {
  long result;
  
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result; // Back-calculate AVcc in mV
  return result;
}

// Buffer 1 nutzen und in Flashpage page schreiben
void writetoflash(int page){
  strcat(messageline, int_string); //append to the messagline string

  while (messageline[i] != '\0')
  {
    dflash.Buffer_Write_Byte(1, i, messageline[i]);  //write to buffer 1, 1 byte at the time
    j = i;
    i++;
  }
  i=0;
  dflash.Buffer_Write_Byte(1, j+1, '\0'); //terminate the string in the buffer
  dflash.Buffer_To_Page(1, page); //write the buffer to the memory on page
}

// Buffer 1 nutzen und aus Flashpage page lesen
void printflashpage(int page){
  dflash.Page_To_Buffer(page, 1);//copy page i to the buffer

  for(int j=0;j<100;j++) //32 old depends on the amount of data on the page
                            // testing for a specific charater is also possible FIXME: Auf Lineend reagieren
  {
    Serial.print(dflash.Buffer_Read_Byte(1, j)); //print the buffer data to the serial port
  }
}

void setup() {

  // SS Pin Bug HW 1.0
  pinMode(9,INPUT);
  
  // Jumper Pins init & Abfrage
  pinMode(jpin1,INPUT);
  pinMode(jpin2,INPUT);
  digitalWrite(jpin1, HIGH);
  digitalWrite(jpin2, HIGH);
  if (!digitalRead(jpin1)){
  	mode = 1;
  }
  
  Serial.begin(9600);
  
  // Init Sensors
  Serial.print("Init...");
  initSensors();
  calibratingG = 400;
  
  // 1-Wire Init
  sensors.begin();
  oneWire.reset_search();
  if (!oneWire.search(insideThermometer)) Serial.println("no inTmp");
  if (!oneWire.search(outsideThermometer)) Serial.println("no outTmp");
  sensors.setResolution(insideThermometer, 9);			// set the resolution to 9bit, only then it can measure from -55°C to +125°C 
  sensors.setResolution(outsideThermometer, 9);		        // set the resolution to 9bit, only then it can measure from -55°C to +125°C
  
  // Dataflash Init
  dflash.init(); //initialize the memory (pins are defined in dataflash.cpp)
  Serial.println("done.");
  
}

void loop() {
   while(1){
  	if (mode == 0){
    
          currentTime = micros();
          
          if ((millis()%1000) == 0){                                // alle 10 sec
            flashadress.adress++; 
    
            // request vcc
            voltage = readVcc();
  
            // Update Sensors
            Gyro_getADC();
            ACC_getADC();
            Mag_getADC();
            Baro_update();
            sensors.requestTemperatures();
            
            // String mit messwerten zusammensetzen
            sprintf(messageline,"%04d,%04d,%04d,%04d,%04d,%04d,%04d,%04d,%04d,%04d,%04d,%04d,%04d", millis(), voltage, sensors.getTempC(outsideThermometer), sensors.getTempC(insideThermometer), gyroADC[0], gyroADC[1], gyroADC[2],accADC[0],accADC[1],accADC[2],magADC[0],magADC[1],magADC[2]);
            Serial.println(messageline);
            Serial.println(i2c_errors_count,DEC);
      
            //writetoflash(flashadress.adress);
            //eeprom_write_block((const void*)&flashadress, (void*)0, sizeof(flashadress));
            
            }
    }else{
    	// auf seriellen Eingabe warten
    	if (Serial.available() > 0) {
    		int inByte = Serial.read();

    		switch (inByte) {

			// clear data
    			/*case 'c':    
      			  flashadress.adress = 0;
			  eeprom_write_block((const void*)&flashadress, (void*)0, sizeof(flashadress));
			  Serial.println("reset 0");
      			break;
		        */
			// zurueck in normalen Betriebsmodus
    			case 'n':    
      			  mode = 0;
			  Serial.println("norm op");
      			break;
				
			// Daten auslesen
    			case 'r':    
      			  Serial.println("Dflashdata: ");
                          for(long p=0; p <= flashadress.adress; p++){
			    printflashpage(flashadress.adress);
                          }
      			break;
    	
			default:
      			  // nichts tun
	  		break;
    		} 
    	}
    
    }
  }      
}
