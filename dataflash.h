/*
 * dataflash.h
 *
 *  Created on: 12.03.2012
 *      Author: Administrator
 */

#ifndef DATAFLASH_H_
#define DATAFLASH_H_

#include <avr/io.h>
#include <stdint.h>

#define DATAFLASH_DDR  DDRB
#define DATAFLASH_PORT  PORTB
#define DATAFLASH_PINS  PINB

#define DATAFLASH_NCS 2
#define DATAFLASH_NWP 0
#define DATAFLASH_NRESET 0

//Data Pin FROM Dataflash
#define DATAFLASH_SO 4

//Data Pin TO Dataflash
#define DATAFLASH_SI 3

#define DATAFLASH_SCK 5

#define DF_RESET_HIGH() DATAFLASH_PORT|=(1<<DATAFLASH_NRESET)
#define DF_CS_LOW()  DATAFLASH_PORT&=~(1<<DATAFLASH_NCS)
#define DF_CS_HIGH()  DATAFLASH_PORT|=(1<<DATAFLASH_NCS)

#define DF_WP_LOW()  DATAFLASH_PORT&=~(1<<DATAFLASH_NWP)
#define DF_WP_HIGH()  DATAFLASH_PORT|=(1<<DATAFLASH_NWP)

#define DF_CLOCK_HIGH() DATAFLASH_PORT|=(1<<DATAFLASH_SCK);
#define DF_CLOCK_LOW() DATAFLASH_PORT&=~(1<<DATAFLASH_SCK);
#define DF_DATA_SI_HIGH() DATAFLASH_PORT|=(1<<DATAFLASH_SI);
#define DF_DATA_SI_LOW() DATAFLASH_PORT&=~(1<<DATAFLASH_SI);

#define DF_COMMAND_DEVICE_ID 0x9f
#define DF_COMMAND_SECURITY_REGISTER_READ 0x77
#define DF_COMMAND_STATUS_READ 0xD7

#define DF_COMMAND_MAIN_MEMORY_PAGE_READ 0xD2

#define DF_COMMAND_BUFFER1_WRITE 0x84
#define DF_COMMAND_PAGE_ERASE 0x81
#define DF_COMMAND_BUFFER1_READ 0xD4
#define DF_COMMAND_BUFFER1_TO_MAIN 0x83

#define DF_COMMAND_CHIP_ERASE_1 0xC7
#define DF_COMMAND_CHIP_ERASE_2 0x94
#define DF_COMMAND_CHIP_ERASE_3 0x80
#define DF_COMMAND_CHIP_ERASE_4 0x9A
#define DF_COMMAND_AUTO_PAGE_REWRITE 0x58

void dataflash_init(void);
void dataflash_device_id(uint8_t* buffer);
void dataflash_status_read(uint8_t* buffer);
void dataflash_security_register_read(uint8_t* buffer);
void dataflash_page_read(int page, uint8_t* buffer);
void dataflash_buffer1_write(uint8_t* buffer);
void dataflash_buffer1_read(uint8_t* buffer);
void dataflash_buffer1_to_main(int page);
void dataflash_auto_page_rewrite(int page);
void dataflash_page_erase(int page);
void dataflash_chip_erase(void);
uint8_t dataflash_ready(void);


#endif /* DATAFLASH_H_ */
