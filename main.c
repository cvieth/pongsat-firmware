/*
 Not-Licht Ueberwachung

 fuer Atmega168V/328P

 interner 8 Mhz Quarz geteilt auf 2 Mhz um Energie zu sparen 
 
 Funktionen:
    * es wurden die Stromspar Tips aus der Appnote 4013 groestenteils umgesetzt
    * Unterspannungsabschaltung bei 2,6V -> ADC Wert von TODO: neu messen !
    * Timer 2 erzeugt Interupt alle 1/2 sec (Uhrenquarz)

 Pinbelegung:


 ADCs:

 
 DEBUG Mode:
    * Ausgabe auf serieller Schnittstelle (9,6kBaud)

 TODO:
    * Alles
    * Sensoren abfragen
    * SPI Flash testen

REVISION:
 
 v1.0:  * erstes Release

 */

// Wichtiger Code fuer den watchdog Reset -> AVR Appnote
#define soft_reset()        					\
do                          					\
{                           					\
wdt_enable(WDTO_15MS);  						\
for(;;)                 						\
{                       						\
}                       						\
} while(0)

#include "config.h"
#include <stdlib.h>
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include <avr/power.h>
#include "dataflash.h"
#include "i2c.h"
#include "ITG-3200.h"

#define FOSC 8000000

#define ITG3200_R 0xD1	// ADD pin is pulled low
#define ITG3200_W 0xD0	// So address is 0x68


#define sbi(var, mask)   ((var) |= (uint8_t)(1 << mask))
#define cbi(var, mask)   ((var) &= (uint8_t)~(1 << mask))

///============Function Prototypes=========/////////////////
char ITG3200Read(unsigned char address);
void ITG3200Write(unsigned char address, unsigned char data);
void ITG3200ViewRegisters(void);
void getITG3200();
void ITG3200BlockRead(unsigned char address);
void adcinit();
void uart_init();
uint16_t getadc(uint8_t channel,uint8_t iref);
void wdt_init(void) __attribute__((naked)) __attribute__((section(".init3")));

///============General Use Prototypes=====//////////////////
void ioinit(void);
void itginit(void);
void cal_clock(void);
void enable_prr(void);
void enable_pullups(void);
void init_uart(void);
void init_timer2(void);
static int uart_putchar(char c, FILE *stream);
void put_char(unsigned char byte);
uint8_t uart_getchar(void);
void delay_ms(uint16_t x);

volatile int average = 1;
volatile signed int gyrox = 0;
volatile signed int gyroy = 0;
volatile signed int gyroz = 0;

ISR(TIMER2_OVF_vect)
{
    
    // Sensoren auslesen
    getITG3200();
    printf("	%d	%d	%d\n", gyrox, gyroy, gyroz);
    
}

int main(void)
{
    //Zeit zum Initialisieren geben
	wdt_disable();
 
    cal_clock();
    clock_prescale_set(clock_div_4);    // Prescale systemclock to 2 MHz
    
    enable_prr();
    enable_pullups();
    
    init_uart();
    printf("Pongsat v1.0\n");
    
    printf("Init Timer2... ");
    init_timer2();
    printf("done.\n");
    
    //init_adc();
    
    printf("Init Dataflash... ");
    dataflash_init();
    printf("done.\n");
    
    printf("Init I2C BUS... ");
    i2cInit();
	delay_ms(100);
    printf("done.\n");
    
    printf("Init ITG3200... ");
    itginit();
    printf("done.\n");

    set_sleep_mode(SLEEP_MODE_PWR_SAVE); // Set sleep mode to Power-save
    sleep_enable();                      // Enable sleep (possible to put the device into sleep mode when executing the sleep instruction)
    
    sei();                               // Enable interrupts


	while (1)
	{
        sleep_bod_disable();            // Disable bod while in sleep mode
        sleep_cpu();                    // Execute sleep instruction
    }

	return 0;
}


// wdt function Implementation
void wdt_init(void)
{
    MCUSR = 0;
    wdt_disable();
	
    return;
}

void init_adc()
{
    ADMUX = (1<<MUX3);                  //Set mux to internal temp sens.
    ADCSRA = (1<<ADEN);                 //ADC enable
}

void init_timer2()
{
    TCCR2B = (1<<CS20) | (1<<CS22);     //Clock prescaler set to 5, overflow each sec
    TIMSK2 = (1<<TOIE2);                //Enable timer2 overflow interrupt
    ASSR = (1<<AS2);                    //Enable async operation
}

void init_uart()
{
    UCSR0A = (1<<U2X0);                 // Enable U2Xn to get a baud rate with less error margin
	UCSR0B = (1<<TXEN0);	            // Transmitter enable
	UCSR0C = (1<<UCSZ00) | (1<<UCSZ01); // Asynchronous USART | No parity | 1 stopbit | CH size 8-bit
	UBRR0 = 0x0C;	                    // 19200 Baudrate@8Mhz
}


void enable_pullups()
{
    PORTB = 0xFF;                       // Enables internal pull-ups on the IO pins, direction bits are by default set to 0 (input)
    PORTC = 0xFF;                       // The UART tx pin will still work
    PORTD = 0xFF;
}

void enable_prr()
{
    power_timer0_disable();
    power_timer1_disable();
}

void cal_clock()
{
    // OSCCAL = 0x55;                   // Calibrate oscillator to 8MHz <- TODO: muss je atmega neu gelesen werden
}

void long_delay(uint16_t ms) {
    for (; ms>0; ms--) _delay_ms(1);
}

char ITG3200Read(unsigned char address)
{
	char data;
	
	cbi(TWCR, TWEN);	// Disable TWI
	sbi(TWCR, TWEN);	// Enable TWI
	
	i2cSendStart();
	i2cWaitForComplete();
	
	i2cSendByte(ITG3200_W);	// write 0xD2
	i2cWaitForComplete();
	
	i2cSendByte(address);	// write register address
	i2cWaitForComplete();
	
	i2cSendStart();
	
	i2cSendByte(ITG3200_R);	// write 0xD3
	i2cWaitForComplete();
	i2cReceiveByte(FALSE);
	i2cWaitForComplete();
	
	data = i2cGetReceivedByte();	// Get MSB result
	i2cWaitForComplete();
	i2cSendStop();
	
	cbi(TWCR, TWEN);	// Disable TWI
	sbi(TWCR, TWEN);	// Enable TWI
	
	return data;
}

void ITG3200Write(unsigned char address, unsigned char data)
{
	i2cSendStart();
	i2cWaitForComplete();
	
	i2cSendByte(ITG3200_W);	// write 0xB4
	i2cWaitForComplete();
	
	i2cSendByte(address);	// write register address
	i2cWaitForComplete();
	
	i2cSendByte(data);
	i2cWaitForComplete();
	
	i2cSendStop();
}

void getITG3200()
{
	char temp;
	
    while (!(ITG3200Read(INT_S) & 0x01))
            ;
    temp = 0;
    temp = ITG3200Read(GY_H);
    gyroy  = temp << 8;
    gyroy |= ITG3200Read(GY_L);
        
    temp = 0;
    temp = ITG3200Read(GZ_H);
    gyroz = temp << 8;
    gyroz |= ITG3200Read(GZ_L);
		
    temp = 0;
    temp = ITG3200Read(GX_H);
    gyrox  = temp << 8;
    gyrox |= ITG3200Read(GX_L);
    
}

static int uart_putchar(char c, FILE *stream)
{
    if (c == '\n') uart_putchar('\r', stream);
    
    loop_until_bit_is_set(UCSR0A, UDRE0);
    UDR0 = c;
    
    return 0;
}

void put_char(unsigned char byte)
{
	/* Wait for empty transmit buffer */
	while ( !( UCSR0A & (1<<UDRE0)) );
	/* Put data into buffer, sends the data */
	UDR0 = byte;
}

uint8_t uart_getchar(void)
{
    while( !(UCSR0A & (1<<RXC0)) );
    return(UDR0);
}

void itginit(){
    char temp;
    unsigned short dlpf = 0;
    
    ITG3200Write(PWR_M, 0x80);	// Reset to defaults
	ITG3200Write(SMPL, 0x00);	// SMLPRT_DIV = 0
	ITG3200Write(DLPF, 0x18);	// DLPF_CFG = 0, FS_SEL = 3
	ITG3200Write(INT_C, 0x05);	// Generate interrupt when device is ready or raw data ready
	ITG3200Write(PWR_M, 0x00);
	
	delay_ms(1000);
    
    // Set filter to 10 Hz
    dlpf = 5;
    temp = 0b00011000 | dlpf;
    ITG3200Write(DLPF, temp);
}

uint16_t getadc (uint8_t channel,uint8_t iref) {
    
    uint8_t i =0;
    uint32_t temp = 0;
    
	// Channel und Referenz auswaehlen
	if (iref == 1) {
  		ADMUX = channel | _BV(REFS1) | _BV(REFS0);
	}else{
  		ADMUX = channel | _BV(REFS0);
	}
    
	//Make two shots 	
  	// Start conversion
  	ADCSRA |= _BV(ADSC);
    
  	// Warten bis ADC Conversion fertig
  	while (ADCSRA & _BV(ADSC) ) {}
    
    // Start conversion
  	ADCSRA |= _BV(ADSC);
    
  	// Warten bis ADC Conversion fertig
  	while (ADCSRA & _BV(ADSC) ) {}
    
    for (i = 0; i < 8; ++i ){
        
        // Start conversion
        ADCSRA |= _BV(ADSC);
        
        // Warten bis ADC Conversion fertig
        while (ADCSRA & _BV(ADSC) ) {}
        
        temp +=ADCW;
    }
    
  	return (uint16_t)((temp/8));
}
