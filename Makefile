NAME=main
NAME2=dataflash

CC=avr-gcc -Os
CFLAGS=-Wall -mmcu=atmega168p
OBJCPY=avr-objcopy
OFLAGS=-O ihex 

AVRDUDE=avrdude 
AFLAGS=-p m168 -c usbasp
FLASHFLAGS=-u -U flash:w:
FUSES=-B 20 -U lfuse:w:0xe2:m -U hfuse:w:0xde:m -U efuse:w:0xf9:m

all: clean object hex size

install: clean all flash

object: 
	$(CC)  $(CFLAGS) -o $(NAME).elf $(NAME).c $(NAME2).c 

hex: 
	$(OBJCPY) $(OFLAGS) $(NAME).elf $(NAME).hex

flash: 
	$(AVRDUDE) $(AFLAGS) $(FLASHFLAGS)$(NAME).hex

fuses:
	$(AVRDUDE) $(AFLAGS) $(FUSES)

clean: 
	rm -f $(NAME).hex $(NAME).elf $(NAME).o

size:
	avr-size -C --mcu=atmega168 $(NAME).elf
