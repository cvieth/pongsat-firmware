/*
 * dataflash.c
 *
 *  Created on: 12.03.2012
 *      Author: Administrator
 */
#include "config.h"
#include <avr/interrupt.h>
#include <avr/io.h>
#include <util/delay.h>
#include <avr/eeprom.h>
#include <avr/wdt.h>
#include <avr/sleep.h>
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <avr/pgmspace.h>
#include "dataflash.h"

static void dataflash_port_init(void);

static void dataflash_read(uint8_t command_size, uint8_t* data_buffer,
		uint16_t data_size);

static void dataflash_write(uint8_t command_size, uint8_t* data_buffer,
		uint16_t data_size);

static void dataflash_command(uint8_t command_size);

static uint8_t command_buffer[8];

void dataflash_init() {

	dataflash_port_init();

	DF_RESET_HIGH();
	DF_WP_HIGH();

}

void dataflash_port_init(void) {

	DATAFLASH_DDR = (1 << DATAFLASH_NWP)
			| (1 << DATAFLASH_NRESET) | (1 << DATAFLASH_NCS)
			| (1 << DATAFLASH_SCK) | (1 << DATAFLASH_SI);

}

void dataflash_device_id(uint8_t* buffer) {

	command_buffer[0] = DF_COMMAND_DEVICE_ID;
	dataflash_read(1, buffer, 4);

}

void dataflash_security_register_read(uint8_t* buffer) {

	command_buffer[0] = DF_COMMAND_SECURITY_REGISTER_READ;
	command_buffer[1] = 0;
	command_buffer[2] = 0;
	command_buffer[3] = 0;
	dataflash_read(4, buffer, 128);

}

void dataflash_status_read(uint8_t* buffer) {

	command_buffer[0] = DF_COMMAND_STATUS_READ;

	dataflash_read(1, buffer, 1);

}

void dataflash_page_read(int page, uint8_t* buffer) {

	uint8_t page_low, page_high;
	page <<= 1;
	page_low = page;
	page_high = page >> 8;

	command_buffer[0] = DF_COMMAND_MAIN_MEMORY_PAGE_READ;
	command_buffer[1] = page_high;
	command_buffer[2] = page_low;
	command_buffer[3] = 0;

	//4 dont-care Bytes

	command_buffer[4] = 0;
	command_buffer[5] = 0;
	command_buffer[6] = 0;
	command_buffer[7] = 0;

	dataflash_read(8, buffer, 256);

}

void dataflash_chip_erase(void) {

	command_buffer[0] = DF_COMMAND_CHIP_ERASE_1;
	command_buffer[1] = DF_COMMAND_CHIP_ERASE_2;
	command_buffer[2] = DF_COMMAND_CHIP_ERASE_3;
	command_buffer[3] = DF_COMMAND_CHIP_ERASE_4;

	dataflash_command(4);

}

void dataflash_buffer1_to_main(int page) {

	uint8_t page_low, page_high;
	page <<= 1;
	page_low = page;
	page_high = page >> 8;

	command_buffer[0] = DF_COMMAND_BUFFER1_TO_MAIN;
	command_buffer[1] = page_high;
	command_buffer[2] = page_low;
	command_buffer[3] = 0;

	dataflash_command(4);

}

void dataflash_auto_page_rewrite(int page) {

	uint8_t page_low, page_high;
	page <<= 1;
	page_low = page;
	page_high = page >> 8;

	command_buffer[0] = DF_COMMAND_AUTO_PAGE_REWRITE;
	command_buffer[1] = page_high;
	command_buffer[2] = page_low;
	command_buffer[3] = 0;

	dataflash_command(4);

}

void dataflash_page_erase(int page) {

	uint8_t page_low, page_high;
	page <<= 1;
	page_low = page;
	page_high = page >> 8;

	command_buffer[0] = DF_COMMAND_PAGE_ERASE;
	command_buffer[1] = page_high;
	command_buffer[2] = page_low;
	command_buffer[3] = 0;

	dataflash_command(4);

}

void dataflash_buffer1_write(uint8_t* buffer) {

	command_buffer[0] = DF_COMMAND_BUFFER1_WRITE;
	command_buffer[1] = 0;
	command_buffer[2] = 0;
	command_buffer[3] = 0;

	dataflash_write(4, buffer, 256);

}

void dataflash_buffer1_read(uint8_t* buffer) {

	command_buffer[0] = DF_COMMAND_BUFFER1_READ;
	command_buffer[1] = 0;
	command_buffer[2] = 0;
	command_buffer[3] = 0;

	dataflash_read(4, buffer, 256);

}

void dataflash_read(uint8_t command_size, uint8_t* data_buffer,
		uint16_t data_size) {

	uint8_t command;
    uint8_t command_count = 0;
    int data_count = 0;
    uint8_t i = 0;

	DF_CS_LOW();

	for (command_count = 0; command_count < command_size; command_count++) {
		command = command_buffer[command_count];

		for (i = 0; i < 8; i++) {

			if (command & 0x80)
				DF_DATA_SI_HIGH()
			else
				DF_DATA_SI_LOW();
			command <<= 1;

			DF_CLOCK_HIGH();


			DF_CLOCK_LOW();

		}

	}

	for (data_count = 0; data_count < data_size; data_count++) {
		data_buffer[data_count] = 0;

		for (i = 0; i < 8; i++) {

			data_buffer[data_count] <<= 1;

			DF_CLOCK_HIGH();

			//sampling an der fallenden Flanke!

		   if (DATAFLASH_PINS & (1 << DATAFLASH_SO))
				data_buffer[data_count] |= 1;

			DF_CLOCK_LOW();


		}

	}

	DF_CS_HIGH();

}

void dataflash_write(uint8_t command_size, uint8_t* data_buffer,
		uint16_t data_size) {

	uint8_t command, data;
    uint8_t command_count = 0;
    uint16_t data_count = 0;
    uint8_t i = 0;

	DF_CS_LOW();

	for (command_count = 0; command_count < command_size;
			command_count++) {
		command = command_buffer[command_count];

		for (i = 0; i < 8; i++) {

			if (command & 0x80)
				DF_DATA_SI_HIGH()
			else
				DF_DATA_SI_LOW();
			command <<= 1;

			DF_CLOCK_HIGH();
			DF_CLOCK_LOW();

		}

	}

	for (data_count = 0; data_count < data_size; data_count++) {
		data = data_buffer[data_count];

		for (i = 0; i < 8; i++) {

			if (data & 0x80)
				DF_DATA_SI_HIGH()
			else
				DF_DATA_SI_LOW();
			data <<= 1;

			DF_CLOCK_HIGH();
			DF_CLOCK_LOW();

		}

	}

	DF_CS_HIGH();

}

void dataflash_command(uint8_t command_size) {

	uint8_t command;
    uint8_t command_count = 0;
    uint8_t i = 0;

	DF_CS_LOW();

	for (command_count = 0; command_count < command_size; command_count++) {
		command = command_buffer[command_count];

		for (i = 0; i < 8; i++) {

			if (command & 0x80)
				DF_DATA_SI_HIGH()
			else
				DF_DATA_SI_LOW();
			command <<= 1;

			DF_CLOCK_HIGH();
			DF_CLOCK_LOW();

		}

	}

	DF_CS_HIGH();

}

uint8_t dataflash_ready(void) {

	uint8_t result[1];

	dataflash_status_read(result);

	return result[0] & 0b10000000;

}

