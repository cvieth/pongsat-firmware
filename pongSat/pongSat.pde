/*
	Pongsat FW Rev. 1.0
	fuer Atmega 1328(P)
	Tacktung: 8 Mhz
	Versorgung aus einer LiPo-Zelle, 4,1 V - 3,0 V, 300mAh
	
	am I2C Bus sind folgende Sensoren:
	* ITG-3200 Gyro		                (2,10 V - 3,60 V, absolut Maximum 6,00 V)
	* BMA180 Beschleunigungssensor		(1,62 V - 3,60 V, absolut Maximum 4,25 V)
	* BMP085 Luftdrucksensor		(1,80 V - 3,60 V, absolut Maximum 4,25 V)
	* HMC5883L Magnetfeldsensor		(2,16 V - 3,60 V, absolut Maximum 4,80 V)
	
	am 1-Wire Bus sind folgende Sensoren:
	* Maxim DS18B20 an Außenhaut            (3,00 V - 5,50 V)
	* Maxim DS18B20 am Lipo                 (3,00 V - 5,50 V)
	
	HW Setup:
	Arduin-Pin	Atmega-Pin	Funktion
	0		PD0		RX
	1		PD1		TX
        2               PD2             1-Wire
        3               PD3             Jumper1
        4               PD4             Jumper2
	10		PB2		CS-Flash
	11		PB3		MOSI
	12		PB4		MISO
	13		PB5		SCK
        14              PC0             Status-LED
	18/ADC4		PC4		SDA
	19/ADC5		PC5		SCL
	
	ADCs:
	* keine
	
	Funktionen:
	* Alle 5 sec Messwerte einlesen und in ext. Flash ablegen
	
	Todo:
	* Code aufrauemen

        Bugs:
        * Datenmuell im Flash -> Erase Funktion selber bauen

*/

#include <Wire.h>
#include <ITG3200.h>
#include <bma180.h>
#include <BMP085.h>
#include <HMC5883L.h>
#include <dataflash.h> // http://arduino.cc/playground/Code/Dataflash
#include <OneWire.h>
#include <DallasTemperature.h>
#include <EEPROM.h>
#include "EEPROMAnything.h"

// Pins Jumper
#define jpin1 3		
#define jpin2 4			
unsigned int mode = 0;

String stringOne;

struct config_t
{
    long adress;
} flashadress;

// BMA180 Defines
#define DEBUGOFF
BMA180 bma180;
unsigned int served=0;

// BMP085 defines
BMP085 bmp;

// HMC5883L defines
HMC5883L compass;
int error = 0;						// Record any errors that may occur in the compass

// Dataflash defines
int j = 0;
int i = 0;
char messageline[200];
char int_string[10];
PROGMEM long pages=4096; 			//total pages that will be used (progmem, ram sparen)
Dataflash dflash; 

// One wire defines
#define ONE_WIRE_BUS 2
#define TEMPERATURE_PRECISION 9
OneWire oneWire(ONE_WIRE_BUS);
DallasTemperature sensors(&oneWire);
DeviceAddress insideThermometer, outsideThermometer;

// ITG 3200 defines
ITG3200 gyro = ITG3200();
float xyz[3], temperature;

//VCC gegen interne 1,1 V Bandgapreferenz messen
long readVcc() {
  long result;
  
  // Read 1.1V reference against AVcc
  ADMUX = _BV(REFS0) | _BV(MUX3) | _BV(MUX2) | _BV(MUX1);
  delay(2); // Wait for Vref to settle
  ADCSRA |= _BV(ADSC); // Convert
  while (bit_is_set(ADCSRA,ADSC));
  result = ADCL;
  result |= ADCH<<8;
  result = 1126400L / result; // Back-calculate AVcc in mV
  return result;
}

// Buffer 1 nutzen und in Flashpage page schreiben
void writetoflash(int page){
  strcat(messageline, int_string); //append to the messagline string

  while (messageline[i] != '\0')
  {
    dflash.Buffer_Write_Byte(1, i, messageline[i]);  //write to buffer 1, 1 byte at the time
    j = i;
    i++;
  }
  i=0;
  dflash.Buffer_Write_Byte(1, j+1, '\0'); //terminate the string in the buffer
  dflash.Buffer_To_Page(1, page); //write the buffer to the memory on page
}

// Buffer 1 nutzen und aus Flashpage page lesen
void printflashpage(int page){
  dflash.Page_To_Buffer(page, 1);//copy page i to the buffer

  for(int j=0;j<180;j++) //32 old depends on the amount of data on the page
                            // testing for a specific charater is also possible FIXME: Auf Lineend reagieren
  {
    Serial.print(dflash.Buffer_Read_Byte(1, j)); //print the buffer data to the serial port
  }
}

// Arduino std Setup
void setup(void) {
  
  // SS Pin Bug HW 1.0
  pinMode(9,INPUT);
  
  // Status LED, an wärend der Initialisierung
  pinMode(14,OUTPUT);
  digitalWrite(14, HIGH);
  
  // Jumper Pins init & Abfrage
  pinMode(jpin1,INPUT);
  pinMode(jpin2,INPUT);
  digitalWrite(jpin1, HIGH);
  digitalWrite(jpin2, HIGH);
  if (!digitalRead(jpin1)){
  	mode = 1;
  }
  
  
  // I2C Init
  Wire.begin();                                                // if experiencing gyro problems/crashes while reading XYZ values, please read class constructor comments for further info.
  delay(300);
  
  // Serial Init
  Serial.begin(9600);
  
  // ITG3200 Init
  gyro.reset(); 
  gyro.init(ITG3200_ADDR_AD0_LOW);				// Use ITG3200_ADDR_AD0_HIGH or ITG3200_ADDR_AD0_LOW as the ITG3200 address
  Serial.print("ITG3200 zeroCalibrating...");
  gyro.zeroCalibrate(2500,2);
  gyro.setFilterBW(BW010_SR1); 					// 10 Hz Filter
  gyro.setPowerMode(NORMAL);   					// Normal Free Running
  Serial.println("done.");
  
  // BMA180 Init
  Serial.print("BMA180 Init...");
  bma180.SetAddress((int)BMA180_DEFAULT_ADDRESS);
  bma180.SoftReset();
  bma180.enableWrite();
  bma180.SetFilter(bma180.F10HZ);
  bma180.setGSensitivty(bma180.G15);
  bma180.SetSMPSkip();
  bma180.disableWrite();
  Serial.println("done.");
  
  // BMP085 Init
  Serial.print("BMP085 Init...");
  bmp.begin();  
  Serial.println("done.");
  
  // HMC5883L Init
  Serial.print("HMC5883L Init...");
  compass = HMC5883L();
  error = compass.SetScale(1.3); // Set the scale to 1.3Ga
  if(error != 0) Serial.println(compass.GetErrorText(error));
  error = compass.SetMeasurementMode(Measurement_Continuous);  // Set the measurement mode to Continuous
  if(error != 0) Serial.println(compass.GetErrorText(error));
  if(error == 0) Serial.println("done.");
    
  // 1-Wire Init
  Serial.print("1Wire Init...");
  sensors.begin();
  oneWire.reset_search();
  if (!oneWire.search(insideThermometer)) Serial.println("Unable to find address for insideThermometer");
  if (!oneWire.search(outsideThermometer)) Serial.println("Unable to find address for outsideThermometer");
  sensors.setResolution(insideThermometer, 9);			// set the resolution to 9bit, only then it can measure from -55°C to +125°C 
  sensors.setResolution(outsideThermometer, 9);		        // set the resolution to 9bit, only then it can measure from -55°C to +125°C
  Serial.println("done.");
  
  // letzte Flash Page aus dem EEPROM lesen
  Serial.print("Read actual Adress from EEPROM... ");
  EEPROM_readAnything(0, flashadress);
  Serial.print(flashadress.adress,DEC);
  Serial.println(" done.");
    
  // Dataflash Init
  Serial.print("Dataflash Init...");
  dflash.init(); //initialize the memory (pins are defined in dataflash.cpp)
  Serial.println("done.");
  
  if (mode == 1){
    Serial.println("System is in command mode:");
  }
  
  digitalWrite(14, LOW);
}

void loop(void){
  long voltage = 0;
  char tempx[12];

  while(1){
  	if (mode == 0){
          if ((millis()%5000) == 0){                                // alle 5 sec
            
            // Schutz vor ueberlauf
            if (flashadress.adress <= 4095){
              flashadress.adress++; 
              
              // request sensor values
              voltage = readVcc();
              sensors.requestTemperatures();
              bma180.readAccel();
              gyro.readGyro(xyz);
              delayMicroseconds(100);		                        // etwas warten bis Sensorwerte ermittelt
              MagnetometerRaw raw = compass.ReadRawAxis();		// not scaled
              MagnetometerScaled scaled = compass.ReadScaledAxis();
    
              // String mit Messwerten zusammensetzen
              stringOne = String(millis());
              stringOne += ",";
              stringOne += voltage;
              stringOne += ",";
              dtostrf(sensors.getTempC(outsideThermometer),7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(sensors.getTempC(insideThermometer),7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(xyz[0],12,7,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(xyz[1],12,7,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(xyz[2],12,7,tempx);
              stringOne += tempx;
              stringOne += ",";
              stringOne += bma180.x;
              stringOne += ",";
              stringOne += bma180.y;
              stringOne += ",";
              stringOne += bma180.z;
              stringOne += ",";
              dtostrf(raw.XAxis,7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(raw.YAxis,7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(raw.ZAxis,7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(bmp.readTemperature(),7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(bmp.readAltitude(),7,2,tempx);
              stringOne += tempx;
              stringOne += ",";
              dtostrf(bmp.readPressure(),9,2,tempx);
              stringOne += tempx;
              stringOne += ";";
            
              // String kuerzen
              stringOne.replace(' ', "");
            
              Serial.println(stringOne);
           
              // String in char array umwandeln fuer Flashwrite
              for (unsigned int a=0;a<=stringOne.length();a++)
              {
                messageline[a]=stringOne[a];
              }
      
              // Daten ins Flash schreiben und Adresse im EEPROM abspeichern 
              writetoflash(flashadress.adress);
              EEPROM_writeAnything(0, flashadress);
              
            }else{
              Serial.println("Error Flash full !");
            }
          }
    }else{
      
    	// auf seriellen Eingabe warten
    	if (Serial.available() > 0) {
    		int inByte = Serial.read();

    		switch (inByte) {

			// clear data
    			case 'c':    
      			  flashadress.adress = 0;
			  EEPROM_writeAnything(0, flashadress);
			  Serial.println("Flash adress reseted to 0.");
      			break;
		
			// zurueck in normalen Betriebsmodus
    			case 'n':    
      			  mode = 0;
			  Serial.println("Back to normal operation.");
      			break;
				
			// Daten auslesen
    			case 'r':    
      			  Serial.println("Read out Data from flash: ");
                          for(long p=1; p <= flashadress.adress; p++){            // sinnvolle daten sind erst ab page 1 zu erwarten 
			    printflashpage(p);
                            Serial.println("");
                          }
      			break;
                        // Flash leeren
    			case 'e': 
                          for(long p=1; p <= 4095; p++){            // sinnvolle daten sind erst ab page 1 zu erwarten 
			    dflash.Page_Erase(p);
                          } 
                          Serial.println("Flash ereased.");
    	                break;

			default:
      			  // nichts tun
	  		break;
    		} 
    	}
    
    }
  }
}
